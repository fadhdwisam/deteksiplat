<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('alamat');
            $table->string('no_plat');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('jenis_kendaraan');
            $table->string('status');
            $table->string('penunggakan');
            $table->string('lama_cicilan');
            $table->string('warna');
            $table->string('type_kendaraan');
            $table->string('merk');
            $table->string('image_plat')->nullable();;
            $table->string('lat')->nullable();;
            $table->string('lon')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
