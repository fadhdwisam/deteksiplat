<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Catagory;
use App\Product;
use Illuminate\Support\Str;
class Category extends Model
{
    protected $fillable = ['name', 'parent_id', 'slug'];
    public function parent()
    {
       
        return $this->belongsTo(Category::class);
    }
    
    
    public function scopeGetParent($query)
    {
       return $query->whereNull('parent_id');
    }
    public function setSlugAttribute($value)
{
    $this->attributes['slug'] = Str::slug($value);
}

    public function getNameAttribute($value)
    {
    return ucfirst($value);
    }
    public function product()
    {
 
    return $this->hasMany(Product::class);
    }
}
