<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Str;
use File;
use App\Exports\ProductExport;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public function product(){
        $product = Product::all();
        return response()->json([
            'message' => 'berhasil',
            'product' => $product
        ], 200);
    }
    public function search($param){
        $post = Product::where('no_plat',$param)->orWhere('name', 'like', '%' . $param . '%')->get();
        if (! $post) {
          return response()->json([
            'message' => 'not found'
          ],404);
        } else {
            return response()->json([
                'message' => 'berhasil',
                'product' => $post
              ],200);
        }

    }
    public function index(){
        
        
           $product = Product::all(); 
    if (request()->q != '') {
        
        $product = $product->where('no_plat', 'LIKE', '%' . request()->q . '%');
    }
    
    //   $product = Product::all()->paginate(5);
    
    return view('products.index', compact('product'));
    }
    public function create(){
        // $category = Category::orderBy('no_plat' , 'DESC');
        $product = Product::all();
        return View('products.create', compact('product'));
    }
    public function storeMobile(Request $request){
       
        return Product::create($request->all());
        
        if($request->hashFile('image_plat')){
            $file = $request->file('image_plat');
            $filename = time() . Str::no_plat($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/products', $filename);
        }
        return redirect(route('product.index'))->with(['success' => 'Data Baru Ditambahkan']);
    }
    public function show($no_plat){
        $post = Product::where('no_plat',$no_plat)->first();
        if (! $post) {
          return response()->json([
            'message' => 'no_plat not found'
          ],404);
        } else {
            return response()->json([
                'message' => 'berhasil',
                'data' => $post
              ],200);
        }

    }
    public function update(Request $request, $no_plat)
    {
        $post = Product::where('no_plat',$no_plat)->first();

        if ($post) {
        $post->update($request->all());
            return response()->json([
                'message' => 'Updated',
                'data' => $post
            ], 200);
        }

        return response()->json([
        'message' => 'No Plat not found',
        ], 404);
    }
    public function delete(Requst $request , $id){
        $product = Product::find($id);

        if ($product) {
            $product->delete($request->all());
            return response()->json([
                'message' => 'delete',
                'data' => $product
            ], 200);
        }else{
            return response()->json([
                'message' => 'Id Not Found',
                ], 404);
        }
    }
    public function store(Request $request){
        Product::create([
            'name' => e($request->input('name')),
            'no_plat' => e($request->input('no_plat')),
            'catagory_id' => e($request->input('catagory_id')), 
            'jenis_kendaraan' => e($request->input('jenis_kendaraan')),
            'lama_cicilan' => e($request->input('lama_cicilan')),
            'image_plat' => e($request->input('image_plat')),
            'type_kendaraan' => e($request->input('type_kendaraan')),
            'alamat' => e($request->input('alamat')),
            'merk' => e($request->input('merk')),
            'status' => e($request->input('status')),
            'lama_cicilan' => e($request->input('lama_cicilan')),
            'penunggakan' => e($request->input('penunggakan')),
            'lat' => e($request->input('lat')),
            'lon' => e($request->input('lon')),
            'warna' => e($request->input('warna'))  

        ]);
                return redirect(route('product.index'))->with(['success' => 'Data Baru Ditambahkan']);
            
    }
    public function destroy($id){
        $product = Product::find($id);
        File::delete(storage_path('app/public/products/' . $product->image_plat));
        $product->delete();
        return redirect(route('product.index'))->with(['success' => 'Produk Sudah Dihapus']);
    }
    public function edit($no_plat){
        $product = Product::find($no_plat);
        return redirect(route('product.edit'))->with(['success' => 'Produk Sudah Dihapus']);
    }
    public function updateProduct(Request $request, $no_plat , $id){
        $product = Product::find($id);
        $product->name = e($request->input('name'));
        $product->no_plat = e($request->input('no_plat'));
        $product->save();
        return redirect()->route('product.plat');
    }
    public function export_excel()
	{
		return Excel::download(new ProductExport, 'Data.xls');
    }
    public function import_excel(Request $request) 
	{
		
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('file_product',$nama_file);
		Excel::import(new ProductImport, public_path('/file_product/'.$nama_file));
		Session::flash('sukses','Data Siswa Berhasil Diimport!');
			return redirect('/product');
	}
}
