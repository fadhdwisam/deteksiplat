<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'nama' => $row[1],
            'no_plat' => $row[2],
            'alamat' => $row[3],
            'jenis_kendaraan' => $row[4],
            'type_kendaraan' => $row[5],
            'status' => $row[6],
            'lama_cicilan' => $row[7],
            'merk' => $row[8],
            'tunggakan' => $row[9],
        ]);
    }
}
