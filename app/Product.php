<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Product extends Model
{
    protected $guarded =[];

    public function setPlatAttribute($value){

    $this->attributes['no_plat'] = Str::no_plat($value); 
    }
    public function getStatusLabelAttribute(){
        if ($this->status == 1){
            return '<span class="badge badge-secondary">Lunas</span>';
        }
        return '<span class="badge badge-success">Belum Lunas</span>';
    }
    public function getStatusLamaCicilanAttribute(){
        if ($this->lama_cicilan == 1){
            return '<span class="badge badge-secondary">12</span>';
        } else if($this->lama_cicilan == 2){
            return '<span class="badge badge-secondary">24</span>';
        } else if($this->lama_cician == 3 ){
            return '<span class="badge badge-secondary">35</span>';
        }else if($this->lama_cicilan == 4){
            return '<span class="badge badge-secondary">42</span>';
        }
        return '<span class="badge badge-secondary">60</span>';
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    protected static $logFillable = true;

    protected $table = "products";
    protected $fillable = [
        'no_plat',
        'lat',
        'lon',
        'image_plat',
        'name',
        'jenis_kendaraan',
        'merk',
        'lama_cicilan',
        'warna',
        'alamat',
        'informasi_id',
        'status',
        'penunggakan',
        'type_kendaraan'
    ];
    

}
