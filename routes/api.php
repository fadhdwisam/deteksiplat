<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('login', 'API\LoginController@index');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'API\UserController@details');
});
Route::group(['prefix' => 'product' ,], function(){
    Route::post('create' , 'ProductController@storeMobile');
    Route::get('show/{no_plat}' , 'ProductController@show');
    Route::post('update/{no_plat}' , 'ProductController@update');
    Route::get('all' , 'ProductController@product');
    Route::delete('delete' ,'ProductController@delete');
    Route::get('search','ProductController@search');
    });
