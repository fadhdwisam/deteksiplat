@extends('layouts.admin')

@section('title')
    <title>Tambah Data</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Data Plat</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Data</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Nama Pemilik</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="description">No_plat</label>
                                    <textarea name="no_plat" id="no_plat" class="form-control">{{ old('no_plat') }}</textarea>
                                    <p class="text-danger">{{ $errors->first('no_plat') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="penunggakan">Penunggakan</label>
                                    <textarea name="penunggakan" id="penunggakan" class="form-control">{{ old('penunggakan') }}</textarea>
                                    <p class="text-danger">{{ $errors->first('penunggakan') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="description">Type Kendaraan</label>
                                    <textarea name="type_kendaraan" id="type_kendaraan" class="form-control">{{ old('type_kendaraan') }}</textarea>
                                    <p class="text-danger">{{ $errors->first('type_kendaraan') }}</p>
                                </div>
                                <div class="form-group">
                                <label for="description">Alamat</label>
                                    <textarea name="alamat" id="alamat" class="form-control">{{ old('alamat') }}</textarea>
                                    <p class="text-danger">{{ $errors->first('alamat') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" class="form-control" required>
                                        <option value="1" {{ old('status') == '1' ? 'selected':'' }}>Lunas</option>
                                        <option value="2" {{ old('status') == '2' ? 'selected':'' }}>Belum Lunas</option>
                                        <option value="0" {{ old('status') == '0' ? 'selected':'' }}>Menunggak</option>
                                    </select>
                                    <p class="text-danger">{{ $errors->first('status') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="lama_cicilan">Lama Cicilan</label>
                                    <select name="lama_cicilan" class="form-control" required>
                                        <option value="1" {{ old('lama_cicilan') == '1' ? 'selected':'' }}>12</option>
                                        <option value="2" {{ old('lama_cicilan') == '2' ? 'selected':'' }}>24</option>
                                        <option value="3" {{ old('lama_cicilan') == '3' ? 'selected':'' }}>35</option>
                                        <option value="4" {{ old('lama_cicilan') == '4' ? 'selected':'' }}>42</option>
                                        <option value="0" {{ old('lama_cicilan') == '0' ? 'selected':'' }}>60</option>
                                    </select>
                                    <p class="text-danger">{{ $errors->first('lama_cicilan') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="price">Jenis Kendaraan</label>
                                    <input type="text" name="jenis_kendaraan" class="form-control" value="{{ old('jenis_kendaraan') }}" required>
                                    <p class="text-danger">{{ $errors->first('jenis_kendaraan') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="merk">Merk
                                    </label>
                                    <input type="text" name="merk" class="form-control" value="{{ old('merk') }}" required>
                                    <p class="text-danger">{{ $errors->first('merk') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="warna">Warna
                                    </label>
                                    <input type="text" name="warna" class="form-control" value="{{ old('warna') }}" required>
                                    <p class="text-danger">{{ $errors->first('warna') }}</p>
                                </div>
                                <div class="form-group text-center">
                                <input type="submit" name="Tambah Data" class="btn btn-primary input-lg" value="Tambah Data" />
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection