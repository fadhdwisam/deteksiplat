@extends('layouts.admin')

@section('title')
    <title>Tambah Data</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Data Plat</li>
    </ol>
    <div class="card-body">
        <from method="post" action="{{ route('product.plat') }}">
            @method('PUT')
            @csrf
            <div class="from-group">
                <label for="name">Name</label>
                <input type="text" value="{{ $product->name }}" name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name">
            </div>
            <div class="from-group">
                <label for="name">Name</label>
                <input type="text" value="{{ $product->no_plaot }}" name="no_plat" class="form-control" id="no_plat" aria-describedby="no_platHelp" placeholder="Enter No Plat">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </from>
    </div>
</main>
@endsection

@section('js')
@endsection