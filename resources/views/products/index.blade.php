@extends('layouts.admin')

@section('title')
    <title>Data Pemilik</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Product</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                            Data Pemilik
                            <div class="float-right">
                                <button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importExcel">
                                    Import Data
                                </button>
                                <a href="{{ route('product.create') }}" class="btn btn-primary btn-sm">Tambah</a>
                                
                                </div>
                            </h4>
                            
                            {{-- notifikasi form validasi --}}
                                @if ($errors->has('file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                                @endif
                        
                                {{-- notifikasi sukses --}}
                                @if ($sukses = Session::get('sukses'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <strong>{{ $sukses }}</strong>
                                </div>
                                @endif
                        <!-- Import Excel -->
                        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <form method="post" action="/product/import_excel" enctype="multipart/form-data">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                                        </div>
                                        <div class="modal-body">
                
                                            <!-- {{ csrf_field() }} -->
                                            @csrf
                
                                            <label>Pilih file excel</label>
                                            <div class="form-group">
                                                <input type="file" name="file" required="required">
                                            </div>
                
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success">{{ session('success') }}</div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif
                            

                            
                            <form action="{{ route('product.index') }}" method="get">
                                <div class="input-group mb-3 col-md-3 float-right">
                                    
                                    <input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="button">Cari</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            
                                            
                                            <th>Plate</th>
                                            <th>Pemilik</th>
                                            <th>Status</th>
                                            <th>Jenis Kendaraan</th>
                                            <th>Merk</th>
                                            <th>Type Kendaraan</th>
                                            <th>Alamat</th>
                                            <th>Lama Cicilan</th>
                                            <th>Tunggakan</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($product ?? ''  as $row)
                                        <tr>
                                           
                                            <td>{{ $row->no_plat }}</td>
                                            <td>
                                                <strong>{{ $row->name }}</strong><br>
                                                <label> <span class="badge badge-info">{{ $row->create}}</span></label><br>
                                            </td>
                                            <td>{{ $row->status }}</td>
                                            <td>{!! $row->jenis_kendaraan !!}</td>
                                            <td>{{ $row->merk }}</td>
                                            <td>{{ $row->type_kendaraan }}</td>
                                            <td>{{ $row->alamat }}</td>
                                            <td>{{ $row->lama_cicilan }}</td>
                                            <td>
                                            {{ $row->penunggakan }}
                                                </td>
                                            <td>
                                                
                                                <form action="{{ route('product.destroy', $row->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <!-- <a href="{{ route('product.edit', $row->no_plat) }}" class="btn btn-warning btn-sm">Edit</a> -->
                                                    <button class="btn btn-danger btn-sm">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="5" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection