@extends('layouts.admin')

@section('title')
    <title>Dashboard</title>
    @endsection
@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Jumlah Data</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="callout callout-info">
                                        <small class="text-muted">Jenis Kendaraan</small>
                                        <br>
                                        <strong class="h4">10</strong>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="callout callout-danger">
                                        <small class="text-muted">Merk</small>
                                        <br>
                                        <strong class="h4">10</strong>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">No plate</small>
                                        <br>
                                        <strong class="h4">10</strong>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Type Kendaraan</small>
                                        <br>
                                        <strong class="h4">10</strong>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="callout callout-success">
                                    <small class="text-muted">Name Pemilik</small>
                                    <br>
                                    <strong class="h4">10</strong>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="callout callout-success">
                                    <small class="text-muted">Alamat Pemilik</small>
                                    <br>
                                    <strong class="h4">10</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
