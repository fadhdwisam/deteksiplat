<nav class="sidebar-nav">
    <ul class="nav">
        <li class="nav-title">MANAJEMEN PLATE</li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-settings"></i> Pengambilan Data
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="/product/export_excel">
                        <i class="nav-icon icon-puzzle"></i> Export Data
                    </a>
                </li>
                
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('product.index') }}">
            <i class="nav-icon icon-drop"></i> Data Pemilik
            </a>
        </li>
    </ul>
</nav>